require 'uri'

class Url < ActiveRecord::Base
  
  validates :longURL, length: {maximum: 1000},
		      presence: { message: "Cannot be blank" },
		      format: {with: /(^$)|([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix,
#Library alternative# format: { with: URI::regexp(%w(http https)),
  		      message: "Invalid URL" } 			
  
  # Generate an array with Latin characters (uppercase & lowercase) and digits
  $symbols = ('a'..'z').to_a + ('A'..'Z').to_a + ('0'..'9').to_a

  def self.tenToSixtyTwo(num)
        if num == 0
                return 0
        end
        remainder = 0
        nums = []

        while (num > 0) 
                remainder = num % 62
                nums.push(remainder)
                num /= 62;
        end

        return nums
  end

  def self.shortExists?(url)
  	Url.exists?(shortURL: url)
  end
	
  def self.encode(longURLArg)
	longURLArg = sanitize(longURLArg)
	
	if Url.exists?(longURL: longURLArg)
                url = Url.select("shortURL").find_by(longURL: longURLArg)
                shorturl = url.shortURL
	else
		url = Url.last
		shorturl = arrToStr(tenToSixtyTwo(url.id))
		Url.create(longURL: longURLArg, shortURL: shorturl)
	end
	
	shorturl
  end
 
  def self.sanitize(url)
  	# Remove "http://" to save space - add it during redirection
	if url.start_with?("http://")
		url = url[7..-1]
	elsif
		url.start_with?("https://")
		url = url[8..-1]
	end
	# Remove '/' if found at the end of the URL
	if url[-1] == "/"
		url = url[0..-2]
	end
	url
  end  

  def self.decode(shortURLArg)
	url = Url.select("longURL").find_by(shortURL: shortURLArg)
  	url.longURL
  end

  def self.arrToStr(nums)
	str=""
	nums.reverse_each {|x| str += $symbols[x]}
	str
  end

  private_class_method :tenToSixtyTwo, :arrToStr, :sanitize

end

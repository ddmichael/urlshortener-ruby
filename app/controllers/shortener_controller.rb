class ShortenerController < ApplicationController

# Welcome action
def index
	if flash[:redirect] == 'redirect to index' 
		flash[:error] = "Invalid URL"
	end
	flash[:redirect] = ""	
end

# This action is being called when a user shortens a URL
def shorten
        @url = params[:url]
	urlz = Url.new
	urlz.longURL = @url
	if !urlz.valid?
		flash[:redirect] = 'redirect to index'
		redirect_to :action => :index
	else
        	@ou = Url.encode(@url)
        	redirect_to root_url + "v/" + @ou
	end
end

# Initially I had only shorten and unshorten actions.
# However redirecting to viewurls gives solution to two problematic scenarios (and probably more):
# 1. Clicking the back button after redirecting to the external page.
# 2. Shortening a URL, clicking 'back' and then 'next'.
# Since going to /shorten requires a post request, using back/next buttons to move there is going to lead
# to no parameters being passed - therefore the page breaks.
def viewurls
@short = params[:shorturl]
        if !Url.shortExists?(@short)
                raise ActionController::RoutingError.new('Not Found')
        end
end

# This action is being called when a user clicks the short URL
def unshorten
        @short = params[:shorturl]
        if Url.shortExists?(@short)
                Url.decode(@short)
        else
                raise ActionController::RoutingError.new('Not Found')
        end
        @long = Url.decode(@short)

        redirect_to "http://" + @long
end

end

class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.text :longURL
      t.text :shortURL

      t.timestamps null: false
    end
  end
end
